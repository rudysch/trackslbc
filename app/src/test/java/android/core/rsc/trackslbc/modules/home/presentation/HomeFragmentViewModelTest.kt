package android.core.rsc.trackslbc.modules.home.presentation

import android.app.Application
import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.utils.ContextHolder
import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.modules.home.domain.usecase.GetTracksUseCase
import android.core.rsc.trackslbc.databuilder.DatabuilderTrackModel
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class HomeFragmentViewModelTest {
    companion object {
        const val RETRY = "RETRY"
    }

    @RelaxedMockK
    private lateinit var application: Application

    private lateinit var contextHolder: ContextHolder

    @MockK
    private lateinit var getTracksUseCase: GetTracksUseCase

    private lateinit var homeFragmentViewModel: HomeFragmentViewModel

    @BeforeEach
    fun before() {
        contextHolder = ContextHolder(application)
        homeFragmentViewModel = spyk(HomeFragmentViewModel(contextHolder, getTracksUseCase))
        every {
            contextHolder.withContext { it.getString(R.string.home_fragment_retry_button_text) }
        } answers { RETRY }
    }

    @Test
    fun testLoadDataAlreadyLoaded() {
        // given
        homeFragmentViewModel.isAlreadyLoaded = true

        // when
        homeFragmentViewModel.load()

        verify(exactly = 0) {
            homeFragmentViewModel.fetchTracksData()
        }
    }

    @Test
    fun testFetchTracksDataFailure() {
        // given
        homeFragmentViewModel.isAlreadyLoaded = false
        coEvery { getTracksUseCase.invoke() } returns Result.Failure()

        // when
        homeFragmentViewModel.load()

        // then
        homeFragmentViewModel.isErrorState.isVisible.value?.let { assertTrue(it) }
    }

    @Test
    fun testFetchTracksDataSuccess() {
        // given
        val tracks = DatabuilderTrackModel.createTracksDataModelList()
        homeFragmentViewModel.isAlreadyLoaded = false
        coEvery { getTracksUseCase.invoke() } returns Result.Success(tracks)

        // when
        homeFragmentViewModel.load()

        // then
        homeFragmentViewModel.trackItems.value?.let { assertEquals(tracks.size, it.size) }
    }
}