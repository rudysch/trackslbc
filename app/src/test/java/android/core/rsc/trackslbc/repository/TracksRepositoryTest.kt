package android.core.rsc.trackslbc.repository

import android.app.Application
import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.utils.ContextHolder
import android.core.rsc.trackslbc.common.utils.NetworkManager
import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks
import android.core.rsc.trackslbc.coreapi.network.TracksApi
import android.core.rsc.trackslbc.database.TrackDao
import android.core.rsc.trackslbc.databuilder.DatabuilderTrack
import android.core.rsc.trackslbc.databuilder.DatabuilderTrackDataModel
import android.core.rsc.trackslbc.modules.home.data.mapper.toModel
import android.core.rsc.trackslbc.modules.home.data.mapper.toTrackModel
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Response

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class TracksRepositoryTest {
    companion object {
        const val FAILURE_DATABASE_MESSAGE = "FAILURE_DATABASE_MESSAGE"
        const val FAILURE_NETWORK_MESSAGE = "FAILURE_NETWORK_MESSAGE"
    }

    @MockK
    private lateinit var tracksApi: TracksApi

    private lateinit var contextHolder: ContextHolder

    @RelaxedMockK
    private lateinit var tracksDao: TrackDao

    @RelaxedMockK
    private lateinit var application: Application

    private lateinit var tracksRepository: TracksRepository

    @BeforeEach
    fun before() {
        contextHolder = ContextHolder(application)
        every { contextHolder.withContext { it.getString(R.string.track_repository_failure_database) } } returns FAILURE_DATABASE_MESSAGE
        every { contextHolder.withContext { it.getString(R.string.track_repository_failure_api) } } returns FAILURE_NETWORK_MESSAGE
        tracksRepository = TracksRepository(tracksApi, tracksDao, contextHolder)
    }

    @Test
    fun testGetTracksOfflineWithoutData() = runTest {
        // given
        every { NetworkManager.isOnline(contextHolder) } returns false
        coEvery { tracksDao.findAll() } returns listOf()

        // when
        val result = tracksRepository.getTracks()

        // then
        assertEquals(Result.Failure(FAILURE_DATABASE_MESSAGE), result)
    }

    @Test
    fun testGetTracksOfflineWithData() = runTest {
        // given
        val tracksData = DatabuilderTrackDataModel.createTracksDataList()
        every { NetworkManager.isOnline(contextHolder) } returns false
        coEvery { tracksDao.findAll() } returns tracksData

        // when
        val result = tracksRepository.getTracks()

        // then
        assertEquals(Result.Success(tracksData.toTrackModel()), result)
    }

    @Test
    fun testGetTracksOnlineWithData() = runTest {
        // given
        val tracks = DatabuilderTrack.createTracksList()
        val expectedAnswer = tracks.toModel()
        every { NetworkManager.isOnline(contextHolder) } returns true
        coEvery { tracksApi.getTracks() } returns Response.success(200, tracks)

        // when
        val result = tracksRepository.getTracks()

        // then
        assertEquals(Result.Success(expectedAnswer), result)
    }
}