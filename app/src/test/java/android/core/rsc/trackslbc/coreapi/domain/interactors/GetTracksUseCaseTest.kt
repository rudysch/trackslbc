package android.core.rsc.trackslbc.coreapi.domain.interactors

import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.coVerify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks
import android.core.rsc.trackslbc.modules.home.domain.usecase.GetTracksUseCase
import io.mockk.coEvery
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
@RunWith(MockitoJUnitRunner::class)
class GetTracksUseCaseTest {
    @MockK
    private lateinit var tracksRepository: ITracks
    private lateinit var getTracksUseCase: GetTracksUseCase

    @BeforeEach
    fun setUp(){
        getTracksUseCase = GetTracksUseCase(tracksRepository)
    }

    @Test
    fun testGetTrackUseCase() = runTest {
        // given
        coEvery { tracksRepository.getTracks() } returns Result.Failure()

        // when
        getTracksUseCase()

        // then
        coVerify (exactly = 1) {
             tracksRepository.getTracks()
        }
    }
}