package android.core.rsc.trackslbc.modules.home.data.mapper

import android.core.rsc.trackslbc.databuilder.DatabuilderTrack
import android.core.rsc.trackslbc.databuilder.DatabuilderTrackDataModel
import android.core.rsc.trackslbc.databuilder.DatabuilderTrackModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TrackMapperTest {

    companion object {
        const val ID = 1
        const val ALBUM_ID = 2
        const val TITLE = "TITLE"
        const val THUMBNAIL_URL = "THUMBNAIL_URL"
        const val URL = "URL"
    }

    @Test
    fun trackToModel() {
        // given
        val track = DatabuilderTrack.create(id = ID, albumId = ALBUM_ID, title = TITLE, thumbnailUrl = THUMBNAIL_URL, url = URL)

        // when
        val result = track.toModel()

        // then
        assertEquals(ID, result.id)
        assertEquals(ALBUM_ID, result.albumId)
        assertEquals(TITLE, result.title)
        assertEquals(THUMBNAIL_URL, result.thumbnailUrl)
        assertEquals(URL, result.url)
    }

    @Test
    fun testTrackToModel() {
        // given
        val track = DatabuilderTrackModel.create(id = ID, albumId = ALBUM_ID, title = TITLE, thumbnailUrl = THUMBNAIL_URL, url = URL)

        // when
        val result = track.toDataModel()

        // then
        assertEquals(ID, result.id)
        assertEquals(ALBUM_ID, result.albumId)
        assertEquals(TITLE, result.title)
        assertEquals(THUMBNAIL_URL, result.thumbnailUrl)
        assertEquals(URL, result.url)
    }

    @Test
    fun testTrackModelToDataModel() {
        // given
        val track = DatabuilderTrackModel.create(id = ID, albumId = ALBUM_ID, title = TITLE, thumbnailUrl = THUMBNAIL_URL, url = URL)

        // when
        val result = track.toDataModel()

        // then
        assertEquals(ID, result.id)
        assertEquals(ALBUM_ID, result.albumId)
        assertEquals(TITLE, result.title)
        assertEquals(THUMBNAIL_URL, result.thumbnailUrl)
        assertEquals(URL, result.url)
    }

    @Test
    fun testTrackDataToModel() {
        // given
        val track = DatabuilderTrackDataModel.create(id = ID, albumId = ALBUM_ID, title = TITLE, thumbnailUrl = THUMBNAIL_URL, url = URL)

        // when
        val result = track.toTrackModel()

        // then
        assertEquals(ID, result.id)
        assertEquals(ALBUM_ID, result.albumId)
        assertEquals(TITLE, result.title)
        assertEquals(THUMBNAIL_URL, result.thumbnailUrl)
        assertEquals(URL, result.url)
    }
}