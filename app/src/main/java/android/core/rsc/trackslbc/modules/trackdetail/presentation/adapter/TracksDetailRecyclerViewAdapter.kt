package android.core.rsc.trackslbc.modules.trackdetail.presentation.adapter

import android.annotation.SuppressLint
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.core.rsc.trackslbc.modules.home.presentation.adapter.holder.TrackItemViewHolder
import android.core.rsc.trackslbc.modules.trackdetail.presentation.adapter.holder.AlbumTrackItemViewHolder
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView

/** Adapter for listing tracks in home page */
class TracksDetailRecyclerViewAdapter(
    val lifecycle: LifecycleOwner,
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<Track>()

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(trackItems: List<Track>) {
        items.clear()
        items.addAll(trackItems)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AlbumTrackItemViewHolder(TrackItemViewHolder.inflate(parent))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? AlbumTrackItemViewHolder)?.bind(lifecycle, items[position])
    }

}