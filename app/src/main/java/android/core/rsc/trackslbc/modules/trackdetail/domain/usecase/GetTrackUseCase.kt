package android.core.rsc.trackslbc.modules.trackdetail.domain.usecase

import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks

/**
 * Retrieve a track
 */
class GetTrackUseCase(private val tracksRepository: ITracks) {
    suspend fun invoke(trackId: Int) = tracksRepository.getTrack(trackId)
}