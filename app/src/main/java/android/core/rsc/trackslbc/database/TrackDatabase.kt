package android.core.rsc.trackslbc.database

import android.core.rsc.trackslbc.database.model.TrackData
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [TrackData::class],
    version = 1, exportSchema = false
)
abstract class TrackDatabase : RoomDatabase() {
    abstract val trackDao: TrackDao
}