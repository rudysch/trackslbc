package android.core.rsc.trackslbc.ui.components.button

import android.core.rsc.trackslbc.ui.generic.ClickableViewData
import android.core.rsc.trackslbc.ui.generic.TextViewData

class ButtonViewData :
    TextViewData by TextViewData.Implementation(),
    ClickableViewData by ClickableViewData.Implementation()