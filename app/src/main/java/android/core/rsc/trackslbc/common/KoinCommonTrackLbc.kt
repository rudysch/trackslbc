package android.core.rsc.trackslbc.common

import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import android.core.rsc.trackslbc.common.utils.ContextHolder
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinCommonTrackLbc : IKoinModule {
    private val utilModule = module {
        single { ContextHolder(get()) }
    }

    override val modules: List<Module> = listOf(utilModule)
}