package android.core.rsc.trackslbc.modules.trackdetail.presentation

import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.common.utils.TAG
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.core.rsc.trackslbc.modules.trackdetail.domain.usecase.GetAlbumTracksUseCase
import android.core.rsc.trackslbc.modules.trackdetail.domain.usecase.GetTrackUseCase
import android.core.rsc.trackslbc.ui.generic.VisibilityViewData
import android.util.Log
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/** ViewModel of [TrackDetailFragment] **/
class TrackDetailFragmentViewModel(
    private val getTrackUseCase: GetTrackUseCase,
    private val getAlbumTracksUseCase: GetAlbumTracksUseCase,
) : ViewModel(), LifecycleObserver {
    val albumTracksItems = MutableLiveData<List<Track>>()
    val isFetchingTrackDataIssue: VisibilityViewData = VisibilityViewData.Implementation()
    val trackTitle = MutableLiveData<String>()
    val trackAlbum = MutableLiveData<String>()

    internal fun fetchTrackData(trackId: Int) {
        isFetchingTrackDataIssue.isVisible.value = false
        viewModelScope.launch(Dispatchers.Default) {
            when (val result = getTrackUseCase.invoke(trackId)) {
                is Result.Failure -> {
                    isFetchingTrackDataIssue.isVisible.value = true
                }
                is Result.Success -> {
                    with(result.data) {
                        trackTitle.postValue(title)
                        trackAlbum.postValue("Album #$albumId")
                        when (val tracks = getAlbumTracksUseCase.invoke(albumId)) {
                            is Result.Failure -> Log.d(TAG, "No tracks for this album")
                            is Result.Success -> albumTracksItems.postValue(tracks.data?: listOf())
                        }
                    }
                }
            }
        }
    }
}