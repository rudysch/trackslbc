package android.core.rsc.trackslbc.modules.home.domain.model

data class Track(val id: Int, val albumId: Int, val title: String, val thumbnailUrl: String, val url: String)