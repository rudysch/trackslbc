package android.core.rsc.trackslbc.coreapi.domain.repository

import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.modules.home.domain.model.Track

/** Repository for Track */
interface ITracks {
    suspend fun getTracks(): Result<List<Track>>
    suspend fun getTrack(trackId: Int): Result<Track>
    suspend fun getAlbumTracks(albumId: Int): Result<List<Track>>
    class UnexpectedApiCode(apiCode: Int) : Exception("Serveur failure, code: $apiCode")
}