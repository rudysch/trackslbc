package android.core.rsc.trackslbc.repository

import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinRepositoryTrackLbc : IKoinModule {
    private val repositoryModule = module {
        single<ITracks> { TracksRepository(get(), get(), get()) }
    }
    override val modules: List<Module> = listOf(repositoryModule)
}