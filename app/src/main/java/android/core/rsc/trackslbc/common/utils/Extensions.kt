package android.core.rsc.trackslbc.common.utils

/**
 * Set a global TAG variable with the className.
 */
val Any.TAG: String
    get() {
        val tag = javaClass.simpleName
        return if (tag.length <= 23) tag else tag.substring(0, 23)
    }