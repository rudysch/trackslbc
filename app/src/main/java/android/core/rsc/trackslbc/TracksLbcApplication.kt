package android.core.rsc.trackslbc

import android.app.Application
import android.core.rsc.trackslbc.common.KoinCommonTrackLbc
import android.core.rsc.trackslbc.coreapi.KoinCoreApiModuleTrackLbc
import android.core.rsc.trackslbc.database.KoinDatabaseTrackLbc
import android.core.rsc.trackslbc.modules.home.KoinHomeModuleTrackLbc
import android.core.rsc.trackslbc.modules.trackdetail.KoinTrackDetailModuleTrackLbc
import android.core.rsc.trackslbc.repository.KoinRepositoryTrackLbc
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.KoinExperimentalAPI
import org.koin.core.context.startKoin

@KoinExperimentalAPI
class TracksLbcApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TracksLbcApplication)
            fragmentFactory()
            modules(KoinCoreApiModuleTrackLbc.modules)
            modules(KoinHomeModuleTrackLbc.modules)
            modules(KoinDatabaseTrackLbc.modules)
            modules(KoinRepositoryTrackLbc.modules)
            modules(KoinCommonTrackLbc.modules)
            modules(KoinTrackDetailModuleTrackLbc.modules)
        }
    }
}