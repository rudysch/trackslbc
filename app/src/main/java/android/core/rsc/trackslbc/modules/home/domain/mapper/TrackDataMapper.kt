package android.core.rsc.trackslbc.modules.home.domain.mapper

import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.core.rsc.trackslbc.ui.components.items.TrackItemViewData

fun List<Track>.toViewData() = this@toViewData.map { it.toTrackViewData() }
fun Track.toTrackViewData() = TrackItemViewData().apply {
    title.text.value = this@toTrackViewData.title
}