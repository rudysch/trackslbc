package android.core.rsc.trackslbc.common.utils

/**
 *
 * Generic result class for error management.
 *
 * Helpers to create Result : [Result.make], [Result.makeSuspended]
 */
sealed class Result<out T> {

    companion object {
        /**
         * Simple helper function to make a [Result] backed by a [Result.Failure] if an exception occur
         * @param maker lambda called to make the result
         */
        inline fun <T> make(maker: () -> Result<T>): Result<T> = try {
            maker()
        } catch (e: Exception) {
            Failure(e.message, e)
        }

        /**
         * Simple helper function to make a [Result] backed by a [Result.Failure] if an exception occur
         * Useful for bloking calls.
         * @param maker lambda called to make the result.
         */
        suspend inline fun <T> makeSuspended(crossinline maker: suspend () -> Result<T>): Result<T> = try {
            maker()
        } catch (e: Exception) {
            Failure(e.message, e)
        }
    }

    /**
     * A success occurred.
     * @param data success data result
     */
    data class Success<out T>(val data: T) : Result<T>()

    /**
     * A Critical failure, this correspond to a system error. Something not intended happened while doing the task.
     * @param message error message
     * @param throwable possible cause of the error
     */
    data class Failure(val message: String? = null, val throwable: Throwable? = null) : Result<Nothing>()
}
