package android.core.rsc.trackslbc.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Tracks")
class TrackData (
    @PrimaryKey val id: Int,
    val albumId: Int,
    val title:String,
    val thumbnailUrl:String,
    val url:String
)

