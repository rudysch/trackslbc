package android.core.rsc.trackslbc.databuilder

import android.core.rsc.trackslbc.coreapi.model.Track

/**
 * Used for UnitTest
 * Build a simple Track object from api
 * Build a list of track from api
 * */
object DatabuilderTrack {
    private const val ALBUM_ID = 1
    private const val ALBUM_ID_2 = 2
    private const val TITLE = "title"
    private const val TITLE_2 = "title2"
    private const val TRACK_ID = 1
    private const val TRACK_ID_2 = 2
    private const val THUMBNAIL_URL = "www.thumbnailurl.com"
    private const val THUMBNAIL_URL_2 = "www.thumbnailurl2.com"
    private const val TRACK_URL = "www.trackurl.com"
    private const val TRACK_URL_2 = "www.trackurl.com"

    fun create(
        albumId: Int = ALBUM_ID,
        id: Int = TRACK_ID,
        thumbnailUrl: String = THUMBNAIL_URL,
        title: String = TITLE,
        url: String = TRACK_URL,
    ) = Track(albumId = albumId, id = id, thumbnailUrl = thumbnailUrl, title = title, url = url)

    fun createTracksList() = listOf(Track(ALBUM_ID, TRACK_ID, THUMBNAIL_URL, TITLE, TRACK_URL),
        Track(ALBUM_ID_2, TRACK_ID_2, THUMBNAIL_URL_2, TITLE_2, TRACK_URL_2))
}