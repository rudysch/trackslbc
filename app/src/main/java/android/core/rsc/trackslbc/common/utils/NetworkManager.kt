package android.core.rsc.trackslbc.common.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities

/**
 * Manage the network status
 *  return true if the devise is online
 *  return false if not
 */
object NetworkManager {
    fun isOnline(contextHolder: ContextHolder): Boolean {
        var result = false
        val connectivityManager = contextHolder.withContext { it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager? }
        connectivityManager?.run {
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                    else -> false
                }
            }
        }
        return result
    }
}
