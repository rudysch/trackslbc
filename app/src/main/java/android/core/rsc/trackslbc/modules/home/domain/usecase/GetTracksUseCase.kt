package android.core.rsc.trackslbc.modules.home.domain.usecase

import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks

/**
 * Retrieve all tracks
 */
class GetTracksUseCase(private val tracksRepository: ITracks) {
    suspend operator fun invoke() = tracksRepository.getTracks()
}