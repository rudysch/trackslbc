package android.core.rsc.trackslbc.repository

import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.utils.ContextHolder
import android.core.rsc.trackslbc.common.utils.NetworkManager.isOnline
import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.common.utils.TAG
import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks
import android.core.rsc.trackslbc.coreapi.network.TracksApi
import android.core.rsc.trackslbc.database.TrackDao
import android.core.rsc.trackslbc.database.model.TrackData
import android.core.rsc.trackslbc.modules.home.data.mapper.toDataModel
import android.core.rsc.trackslbc.modules.home.data.mapper.toModel
import android.core.rsc.trackslbc.modules.home.data.mapper.toTrackModel
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * TrackRepository return a Result<List<Track>> depends on online status
 * From tracksApi if network Online
 * From database if network offline
 * If there is no data from the network or the database a Result.Failure() is send
 */
class TracksRepository(
    private val tracksApi: TracksApi,
    private val trackDao: TrackDao,
    private val contextHolder: ContextHolder,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
) : ITracks {
    override suspend fun getTracks(): Result<List<Track>> {
        return if (isOnline(contextHolder)) Result.makeSuspended api@{
            Log.d(TAG, "From network")
            val apiResult = tracksApi.getTracks()
            if (apiResult.code() != 200) throw ITracks.UnexpectedApiCode(apiResult.code())
            val body =
                apiResult.body()?.toModel() ?: return@api Result.Failure(contextHolder.withContext { it.getString(R.string.track_repository_failure_api) })
            saveTrackDataFromApi(body)
            Result.Success(body)
        } else {
            Result.makeSuspended database@{
                Log.d(TAG, "From database")
                val data = getTracksDataFromCache()
                if (data.isEmpty()) return@database Result.Failure(contextHolder.withContext { it.getString(R.string.track_repository_failure_database) })
                Result.Success(data.toTrackModel())
            }
        }
    }

    override suspend fun getTrack(trackId: Int): Result<Track> = Result.makeSuspended database@{
        Log.d(TAG, "From database")
        val data = getTrackDataFromCache(trackId)
        if (data == null) return@database Result.Failure(contextHolder.withContext { it.getString(R.string.track_detail_fragment_no_data_message) })
        Result.Success(data.toTrackModel())
    }

    override suspend fun getAlbumTracks(albumId: Int): Result<List<Track>> = Result.makeSuspended database@{
        Log.d(TAG, "From database")
        val data = getAlbumTracksDataFromCache(albumId)
        if (data.isEmpty()) return@database Result.Failure(contextHolder.withContext { it.getString(R.string.track_repository_failure_database) })
        Result.Success(data.toTrackModel())
    }

    suspend fun getTracksDataFromCache(): List<TrackData> = withContext(dispatcher) { trackDao.findAll() }

    suspend fun getTrackDataFromCache(trackId: Int): TrackData = withContext(dispatcher) { trackDao.findTrack(trackId) }

    suspend fun getAlbumTracksDataFromCache(albumTrackId: Int): List<TrackData> = withContext(dispatcher) { trackDao.findAlbumTracks(albumTrackId) }

    suspend fun saveTrackDataFromApi(tracks: List<Track>) = withContext(dispatcher) { trackDao.addTracks(tracks.toDataModel()) }
}
