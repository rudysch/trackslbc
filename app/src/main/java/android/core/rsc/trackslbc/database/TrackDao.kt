package android.core.rsc.trackslbc.database

import android.core.rsc.trackslbc.database.model.TrackData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TrackDao {

    @Query("SELECT * FROM tracks")
    fun findAll(): List<TrackData>

    @Query("SELECT * FROM tracks WHERE id = :id")
    fun findTrack(id: Int): TrackData

    @Query("SELECT * FROM tracks WHERE albumId = :albumId")
    fun findAlbumTracks(albumId:Int): List<TrackData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTracks(tracks: List<TrackData>)
}