package android.core.rsc.trackslbc.modules.home.presentation

import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.BaseFragment
import android.core.rsc.trackslbc.databinding.FragmentHomeBinding
import android.core.rsc.trackslbc.modules.home.presentation.adapter.TracksRecyclerViewAdapter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.android.viewmodel.ext.android.viewModel

/** Fragment of [R.layout.fragment_home] **/
class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    private val viewModel: HomeFragmentViewModel by viewModel()

    override val layoutId: Int
        get() = R.layout.fragment_home

    override fun bindViewModels(binding: FragmentHomeBinding) {
        binding.viewModel = viewModel
        binding.lifecycle = lifecycle
        lifecycle.lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setLiveDataObservers()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.load()
    }

    private fun setLiveDataObservers() {
        viewModel.trackItems.observe(viewLifecycleOwner) {
            ((binding.tracksRecyclerView.adapter) as TracksRecyclerViewAdapter).updateData(it)
        }
    }

    private fun initRecyclerView() {
        binding.tracksRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = TracksRecyclerViewAdapter(viewLifecycleOwner)
        }
    }
}
