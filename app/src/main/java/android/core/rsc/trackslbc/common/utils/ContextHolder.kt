package android.core.rsc.trackslbc.common.utils

import android.app.Activity
import android.app.Application
import android.content.Context

/**
 * ContextHolder provide the possibility to add a new context and to remove the present context
 * Avoid the context issue in the  application
 */
class ContextHolder(val application: Application) {
    var activity: Activity? = null

    fun addActivityContext(activity: Activity) {
        this.activity = activity
    }

    fun removeActivityContext(activity: Activity) {
        if (activity == this.activity) {
            this.activity = null
        }
    }

    /**
     * If context activity is present take it, otherwise take the application context
     */
    fun <T> withContext(action: (Context) -> T): T = action(activity ?: application)
}