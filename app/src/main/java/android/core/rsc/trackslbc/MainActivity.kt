package android.core.rsc.trackslbc

import android.core.rsc.trackslbc.common.utils.ContextHolder
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.inject

/**
 * MainActivity initilize the contextHolder and destroy it when the app was kill
 */
class MainActivity : AppCompatActivity() {
    private val contextHolder: ContextHolder by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contextHolder.addActivityContext(this)
        setContentView(R.layout.activity_main)
    }

    override fun onDestroy() {
        contextHolder.removeActivityContext(this)
        super.onDestroy()
    }
}