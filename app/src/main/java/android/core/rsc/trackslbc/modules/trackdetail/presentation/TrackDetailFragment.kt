package android.core.rsc.trackslbc.modules.trackdetail.presentation

import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.BaseFragment
import android.core.rsc.trackslbc.databinding.FragmentTrackDetailBinding
import android.core.rsc.trackslbc.modules.trackdetail.presentation.adapter.TracksDetailRecyclerViewAdapter
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.android.viewmodel.ext.android.viewModel

/** Fragment of [R.layout.fragment_track_detail] **/
class TrackDetailFragment : BaseFragment<FragmentTrackDetailBinding>() {
    private val viewModel: TrackDetailFragmentViewModel by viewModel()

    override val layoutId: Int
        get() = R.layout.fragment_track_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        setLiveDataObservers()
    }

    override fun bindViewModels(binding: FragmentTrackDetailBinding) {
        binding.viewModel = viewModel
        binding.lifecycle = lifecycle
        lifecycle.lifecycle.addObserver(viewModel)

        retrieveScanType()
    }

    private fun retrieveScanType() {
        val bundle = arguments ?: return
        if (arguments == null){
            viewModel.isFetchingTrackDataIssue.isVisible.value = true
        }else{
            viewModel.fetchTrackData(TrackDetailFragmentArgs.fromBundle(bundle).trackId)
        }
    }

    private fun setLiveDataObservers() {
        viewModel.albumTracksItems.observe(viewLifecycleOwner) {
            ((binding.albumTracksRecyclerview.adapter) as TracksDetailRecyclerViewAdapter).updateData(it)
        }
    }

    private fun initRecyclerView() {
        binding.albumTracksRecyclerview.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = TracksDetailRecyclerViewAdapter(viewLifecycleOwner)
        }
    }
}
