package android.core.rsc.trackslbc.ui.components.items

import android.core.rsc.trackslbc.ui.generic.ClickableViewData
import android.core.rsc.trackslbc.ui.generic.TextViewData

/**Track item for recyclerView at home page*/
class TrackItemViewData: ClickableViewData by ClickableViewData.Implementation() {
    val title = TextViewData.Implementation()
}