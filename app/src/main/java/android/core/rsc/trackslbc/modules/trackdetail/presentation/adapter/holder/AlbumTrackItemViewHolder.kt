package android.core.rsc.trackslbc.modules.trackdetail.presentation.adapter.holder

import android.core.rsc.trackslbc.databinding.UiTrackItemBinding
import android.core.rsc.trackslbc.modules.home.domain.mapper.toTrackViewData
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.core.rsc.trackslbc.modules.home.presentation.HomeFragmentDirections
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

/**
 *  Holder for [android.core.rsc.trackslbc.modules.home.presentation.adapter.TracksRecyclerViewAdapter]
 *  Holder of TrackItem
 */
class AlbumTrackItemViewHolder(private val binding: UiTrackItemBinding) : RecyclerView.ViewHolder(binding.root) {
    init {
        itemView.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT).apply {
            this.topMargin = 20
        }
    }

    companion object {
        fun inflate(parent: ViewGroup) = UiTrackItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    fun bind(lifecycle: LifecycleOwner, item: Track) {
        val track = item.toTrackViewData()
        binding.apply {
            viewData = track
            lifecycleOwner = lifecycle
        }
    }
}