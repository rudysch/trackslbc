package android.core.rsc.trackslbc.modules.trackdetail.domain.usecase

import android.core.rsc.trackslbc.coreapi.domain.repository.ITracks

/**
 * Retrieve all tracks from same album
 */
class GetAlbumTracksUseCase(private val tracksRepository: ITracks) {
    suspend operator fun invoke(albumTracksId: Int) = tracksRepository.getAlbumTracks(albumTracksId)
}