package android.core.rsc.trackslbc.common

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner

abstract class BaseView<VB : ViewDataBinding> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    /** The lifecycleOwner of the view. */
    open var lifecycle: LifecycleOwner?
        get() = binding.lifecycleOwner
        set(value) {
            binding.lifecycleOwner = value
            binding.setVariable(BR.lifecycle, lifecycle)
            value?.let(this::onLifeCycleOwnerSet)
        }

    /**
     * The ViewDataBinding used by the view.
     * Since it use [layoutId] which is abstract at init time, it raises a Warning.
     * */
    lateinit var binding: VB

    /** For customView who want to get a notification when lifecycleOwner is set */
    open fun onLifeCycleOwnerSet(lifecycleOwner: LifecycleOwner) = Unit

    /** The resource layout Id used by extended view */
    abstract val layoutId: Int

    init {
        if (!isInEditMode) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), layoutId, this, true)
        } else {
            LayoutInflater.from(context).inflate(layoutId, this, true)
        }
        // Disable clipping for child view.
        // A cardView shadow will be clip if those variables are not set to false
        clipToPadding = false
        clipChildren = false
    }
}