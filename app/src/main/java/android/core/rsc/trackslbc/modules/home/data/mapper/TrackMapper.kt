package android.core.rsc.trackslbc.modules.home.data.mapper

import android.core.rsc.trackslbc.coreapi.model.Track
import android.core.rsc.trackslbc.database.model.TrackData
import android.core.rsc.trackslbc.modules.home.domain.model.Track as TrackModel

fun List<Track>.toModel() = this@toModel.map { it.toModel() }
fun Track.toModel() =
    TrackModel(id, albumId, title, thumbnailUrl, url)

fun List<TrackModel>.toDataModel() = this@toDataModel.map { it.toDataModel() }
fun TrackModel.toDataModel() = TrackData(id, albumId, title, thumbnailUrl, url)

fun List<TrackData>.toTrackModel() = this@toTrackModel.map { it.toTrackModel() }
fun TrackData.toTrackModel() =
    TrackModel(id, albumId, title, thumbnailUrl, url)