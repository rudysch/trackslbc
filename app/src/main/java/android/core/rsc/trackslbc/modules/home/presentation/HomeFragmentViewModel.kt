package android.core.rsc.trackslbc.modules.home.presentation

import android.core.rsc.trackslbc.R
import android.core.rsc.trackslbc.common.utils.ContextHolder
import android.core.rsc.trackslbc.common.utils.Result
import android.core.rsc.trackslbc.modules.home.domain.usecase.GetTracksUseCase
import android.core.rsc.trackslbc.modules.home.domain.model.Track
import android.core.rsc.trackslbc.ui.components.button.ButtonViewData
import android.core.rsc.trackslbc.ui.generic.TextViewData
import android.core.rsc.trackslbc.ui.generic.VisibilityViewData
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/** ViewModel of [HomeFragment] **/
class HomeFragmentViewModel(
    private val contextHolder: ContextHolder,
    private val getTracksUseCase: GetTracksUseCase,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
) : ViewModel(), LifecycleObserver {
    val trackItems = MutableLiveData<List<Track>>()
    val loading: VisibilityViewData = VisibilityViewData.Implementation()
    val isErrorState: VisibilityViewData = VisibilityViewData.Implementation()
    val errorMessage: TextViewData = TextViewData.Implementation()
    var isAlreadyLoaded: Boolean = false

    /**
     * Initialize retry button
     */
    val retryButton = ButtonViewData().apply {
        text.postValue(contextHolder.withContext { it.getString(R.string.home_fragment_retry_button_text) } )
        enabled.postValue(true)
        onClick = {
            fetchTracksData()
        }
    }

    /**
     * Manage if data are already loaded
     */
    fun load() {
        if (!isAlreadyLoaded) fetchTracksData()
    }

    fun fetchTracksData() {
        viewModelScope.launch(dispatcher) {
            loading.isVisible.postValue(true)
            isErrorState.isVisible.postValue(false)
            when (val result = getTracksUseCase()) {
                is Result.Failure -> {
                    errorMessage.text.postValue(result.message)
                    isErrorState.isVisible.postValue(true)
                }
                is Result.Success -> {
                    trackItems.postValue(result.data ?: emptyList())
                    isAlreadyLoaded = true
                }
            }
            loading.isVisible.postValue(false)
        }
    }
}