package android.core.rsc.trackslbc.database

import android.app.Application
import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinDatabaseTrackLbc : IKoinModule {
    private val databaseModule = module {

        fun provideDatabase(application: Application): TrackDatabase {
            return Room.databaseBuilder(application, TrackDatabase::class.java, "tracks")
                .fallbackToDestructiveMigration()
                .build()
        }

        fun provideCountriesDao(database: TrackDatabase): TrackDao {
            return  database.trackDao
        }

        single { provideDatabase(androidApplication()) }
        single { provideCountriesDao(get()) }


    }

    override val modules: List<Module> = listOf(databaseModule)
}