package android.core.rsc.trackslbc.modules.trackdetail

import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import android.core.rsc.trackslbc.modules.trackdetail.domain.usecase.GetAlbumTracksUseCase
import android.core.rsc.trackslbc.modules.trackdetail.domain.usecase.GetTrackUseCase
import android.core.rsc.trackslbc.modules.trackdetail.presentation.TrackDetailFragment
import android.core.rsc.trackslbc.modules.trackdetail.presentation.TrackDetailFragmentViewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinTrackDetailModuleTrackLbc : IKoinModule {
    private val trackDetailModule = module {
        fragment { TrackDetailFragment() }
        viewModel { TrackDetailFragmentViewModel(get(), get()) }
    }

    private val trackDetailUseCases = module {
        single { GetTrackUseCase(get()) }
        single { GetAlbumTracksUseCase(get()) }
    }

    override val modules: List<Module> = listOf(trackDetailModule, trackDetailUseCases)
}