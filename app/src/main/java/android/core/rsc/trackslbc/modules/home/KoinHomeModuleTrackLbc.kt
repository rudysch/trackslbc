package android.core.rsc.trackslbc.modules.home

import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import android.core.rsc.trackslbc.modules.home.domain.usecase.GetTracksUseCase
import android.core.rsc.trackslbc.modules.home.presentation.HomeFragment
import android.core.rsc.trackslbc.modules.home.presentation.HomeFragmentViewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinHomeModuleTrackLbc : IKoinModule {
    private val homeModule = module {
        fragment { HomeFragment() }
        viewModel { HomeFragmentViewModel(get(), get()) }
    }

    val homeDetailUseCases = module {
        single { GetTracksUseCase(get()) }
    }

    override val modules: List<Module> = listOf(homeModule, homeDetailUseCases)
}