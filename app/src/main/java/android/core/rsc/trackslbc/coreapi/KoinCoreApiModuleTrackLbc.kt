package android.core.rsc.trackslbc.coreapi

import android.core.rsc.trackslbc.common.interfaces.IKoinModule
import android.core.rsc.trackslbc.coreapi.network.provideLoggingInterceptor
import android.core.rsc.trackslbc.coreapi.network.provideOkHttpClient
import android.core.rsc.trackslbc.coreapi.network.provideRetrofit
import android.core.rsc.trackslbc.coreapi.network.provideTracksApi
import org.koin.core.module.Module
import org.koin.dsl.module

object KoinCoreApiModuleTrackLbc : IKoinModule {
    private val coreapiModule = module {
        factory { provideOkHttpClient(get()) }
        factory { provideTracksApi(get()) }
        factory { provideLoggingInterceptor() }
        single { provideRetrofit(get()) }
    }
    override val modules: List<Module> = listOf(coreapiModule)
}