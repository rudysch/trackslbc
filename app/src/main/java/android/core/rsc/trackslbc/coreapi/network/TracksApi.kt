package android.core.rsc.trackslbc.coreapi.network

import android.core.rsc.trackslbc.coreapi.model.Track
import retrofit2.Response
import retrofit2.http.GET

/**
 * Api that retrieve all the tracks from network
 */
interface TracksApi {
    @GET("technical-test.json")
    suspend fun getTracks(): Response<List<Track>>
}