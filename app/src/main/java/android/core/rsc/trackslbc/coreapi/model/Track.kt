package android.core.rsc.trackslbc.coreapi.model

/**
 * Track model from api
 */
data class Track(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String,
)
