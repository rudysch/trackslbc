package android.core.rsc.trackslbc.ui.generic

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map

interface VisibilityViewData {
    val isVisible: MutableLiveData<Boolean>
    val visibilityInt: LiveData<Int>
    val invertedVisilibilityInt: LiveData<Int>

    open class Implementation(visible: Boolean = true) : VisibilityViewData {
        override val isVisible: MutableLiveData<Boolean> = MutableLiveData(visible)
        override val visibilityInt: LiveData<Int> by lazy { isVisible.map { if (it) View.VISIBLE else View.GONE } }
        override val invertedVisilibilityInt: LiveData<Int> by lazy { isVisible.map { if (it) View.GONE else View.VISIBLE } }
    }

    companion object {

        @JvmStatic
        @BindingAdapter("view_is_visible")
        fun setVisibility(view: View, isVisible: Boolean?) {
            view.isVisible = isVisible ?: false
        }
    }
}