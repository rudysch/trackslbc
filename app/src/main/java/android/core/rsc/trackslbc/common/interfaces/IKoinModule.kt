package android.core.rsc.trackslbc.common.interfaces

interface IKoinModule {
    val modules: List<org.koin.core.module.Module>
}