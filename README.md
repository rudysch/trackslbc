# TracksLbc

Application permettant l'affichage de titres d'albums (Vidéo dans le package documentation)

<img src="/documentation/home_first_connection_offline.PNG" width="200" height="400" />
<img src="/documentation/home_portrait.PNG" width="200" height="400" />
<img src="/documentation/home_landscape.PNG" width="400" height="200" />
<img src="/documentation/track_detail_portrait.PNG" width="200" height="400" />

## Contexte

Vous devez réaliser une application native Android affichant la liste de titres d'albums
suivant : https://static.leboncoin.fr/img/shared/technical-test.json
Les données contenues dans ce json doivent être téléchargées par l'app au Runtime, et non
mises manuellement dans les assets de l'app.

## Fonctionnement
Si l'utilisateur lance l'application sans réseau (WIFI, VPN SIM etc...) il aura un message d'erreur et un bouton qui l'invitera à recommencer.
Tant que l'utilisateur a une connexion internet, les données seront récupées via un call api et sauvegarder en local via Room.
Au cas ou, l'utilisateur quitte l'application et l'a réouvre sans avoir de réseau alors il retrouvera les dernières données téléchargé localement.

## Librairies
- Navigation -> Jetpack navigation pour faciliter la navigation entre fragments (v2.3.5)
- Koin -> Framework kotlin d'injection de dépendance (2.2.3)
- Retrofit -> Http client pour Android
- Room -> Utiliser pour la persistance des données
- jupiter -> Librairie pour les tests unitaires

## Architecture
Mise en place d'une clean architecture avec la division des packages et/ou modules en data/domain/presentation.
Le pattern utilisé pour la réalisation de ce test a été le pattern MVVM(Modèle-vue-vue-modèle).

Une partie ui ou design system a été rapidement mis en place avec l'utilisation de composant générique comme le TextViewData, le ClickableViewData ou le VisibilityViewData.
Ils permettent d'initialiser rapidement des nouveaux composants sans avoir de duplicata de code.
Deux composants ont été créés:
- ButtonViewData -> utilisé pour le bouton "réessayer"
- TrackItemViewData -> utilisé pour afficher les titres dans la recyclerView

## Tests
- Interactor -> GetTracksUseCasTest
- Mapper -> TrackerMapper
- ViewModel -> HomeFragmentViewModel
- repository -> TracksRepository

## To do
:warning: Gérer le duplicata de données pour la thématisation avec le Common pour le darkMode et le lightMode
:warning: Gérer la self navigation du fragment detail tracks (duplicata du reclerView)